package main

import (
	"github.com/sirupsen/logrus"
	"github.com/urfave/cli"
	"gitlab.com/72th/iso20022-pain001/pkg/convert"
	"gitlab.com/72th/iso20022-pain001/pkg/pain001"
	"os"
)

func main() {
	app := cli.NewApp()
	app.Name = "sba-pay"
	app.Usage = "convert a json file to pain.001 payment orders"
	app.Version = "0.1.1"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name: "output, o",
			Usage: "path tho output file",
		},
	}
	app.Action = func(c *cli.Context) error {
		if len(c.Args()) == 0 {
			logrus.Fatal("please specify a file path")
		} else if c.String("output") == "" {
			logrus.Fatal("please specify a output path with -o")
		}
		dcm := pain001.NewDocument(*convert.OpenData(c.Args().First()))
		dcm.Save(c.String("output"))
		return nil
	}
	app.Commands = []cli.Command{
		{
			Name: "create",
			Action: func(c *cli.Context) error {
				if len(c.Args()) == 0 {
					logrus.Fatal("Please specify a path")
				}
				data := convert.NewData()
				data.Save(c.Args().First())
				return nil
			},
		},
	}
	if err := app.Run(os.Args); err != nil {
		logrus.Fatal(err)
	}
}
