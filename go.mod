module gitlab.com/72th/sba-pay

go 1.13

require (
	github.com/creasty/defaults v1.3.0
	github.com/satori/go.uuid v1.2.0
	github.com/sirupsen/logrus v1.4.2
	github.com/urfave/cli v1.22.2
)
