package pain001

import (
	"encoding/xml"
	"gitlab.com/72th/iso20022-pain001/pkg/convert"
)

// CustomerCreditTransferInitiation contains all the needed information.
type CustomerCreditTransferInitiation struct {
	XMLName            xml.Name           `xml:"CstmrCdtTrfInitn"`
	GroupHeader        GroupHeader        `xml:"GrpHdr"`
	PaymentInformation PaymentInformation `xml:"PmtInf"`
}

// NewCustomerCreditTransferInitiation takes convert.Data as an argument and returns a new CustomerCreditTransferInitiation.
func NewCustomerCreditTransferInitiation(data convert.Data) CustomerCreditTransferInitiation {
	return CustomerCreditTransferInitiation{
		GroupHeader: NewGroupHeader(data.Sender.Name, len(data.Transactions), data.ControlSum()),
		PaymentInformation: NewPaymentInformation(data),
	}
}
