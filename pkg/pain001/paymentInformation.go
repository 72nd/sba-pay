package pain001

import (
	"gitlab.com/72th/iso20022-pain001/pkg/convert"
	"strings"
)

// PaymentInformation contains most of the payment information. For more information please refer to the standard.
type PaymentInformation struct {
	PaymentInformationId         string                        `xml:"PmtInfId"`
	PaymentMethod                string                        `xml:"PmtMtd"`
	BatchBooking                 bool                          `xml:"BtchBookg"`
	RequiredExecutionDate        string                        `xml:"ReqdExctnDt"`
	Debitor                      Address                       `xml:"Dbtr"`
	DebitorIban                  string                        `xml:"DbtrAcct>Id>IBAN"`
	DebitorBic                   string                        `xml:"DbtrAgt>FinInstnId>BIC"`
	CreditorTransferInformation []CreditorTransferInformation `xml:"CdtTrfTxInf"`
}

// NewPaymentInformation takes convert.Sender as an argument and returns a new PaymentInformation.
func NewPaymentInformation(data convert.Data) PaymentInformation {
	info := PaymentInformation{
		PaymentInformationId:  "PMTINFID-01",
		PaymentMethod:         "TRF",
		BatchBooking:          true,
		RequiredExecutionDate: data.ExecuteOn,
		Debitor: Address{
			Name: data.Sender.Name,
			PostalAddress: PostalAddress{
				StreetName:   data.Sender.Street,
				StreetNumber: data.Sender.StreetNr,
				PostalCode:   data.Sender.Postcode,
				TownName:     data.Sender.Place,
				Country:      data.Sender.Country,
			},
		},
		DebitorIban: strings.Replace(data.Sender.Iban, " ", "", -1),
		DebitorBic:  data.Sender.Bic,
	}

	info.CreditorTransferInformation = make([]CreditorTransferInformation, len(data.Transactions))
	for i := range data.Transactions {
		info.CreditorTransferInformation[i] = NewCreditorTransferInformation(data.Transactions[i], i+1)
	}
	return info
}
