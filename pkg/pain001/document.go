package pain001

import (
	"encoding/xml"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/72th/iso20022-pain001/pkg/convert"
	"io/ioutil"
)

// DateTimeFormat represents the date and time format which is used in the standard.
const DateTimeFormat = "2006-01-02T15:04:05"

// Document is the root structure of the payment statement.
type Document struct {
	XMLName                          xml.Name `xml:"Document"`
	Xmlns                            string   `xml:"xmlns,attr"`
	XmlnsXid                         string   `xml:"xmlns:xsi,attr"`
	XsiSchemaLocation                string   `xml:"xsi:schemaLocation,attr"`
	CustomerCreditTransferInitiation CustomerCreditTransferInitiation
}

// NewDocument returns a new document.
func NewDocument(data convert.Data) Document {
	document := Document{
		Xmlns:                            "http://www.six-interbank-clearing.com/de/pain.001.001.03.ch.02.xsd",
		XmlnsXid:                         "http://www.w3.org/2001/XMLSchema-instance",
		XsiSchemaLocation:                "http://www.six-interbank-clearing.com/de/pain.001.001.03.ch.02.xsd  pain.001.001.03.ch.02.xsd",
		CustomerCreditTransferInitiation: NewCustomerCreditTransferInitiation(data),
	}
	return document
}

// Save produces pain file and saves it to the given path.
func (d Document) Save(path string) {
	output, err := xml.MarshalIndent(d, "  ", "    ")
	if err != nil {
		logrus.Fatal(err)
		return
	}
	output = []byte(xml.Header + string(output))
	if err := ioutil.WriteFile(path, output, 0644); err != nil {
		logrus.Fatal(err)
	}
}

// PrintResult is a test function and just prints the XML for debug purposes.
func (d Document) PrintResult() {
	output, err := xml.MarshalIndent(d, "  ", "    ")
	if err != nil {
		logrus.Error(err)
		return
	}
	fmt.Print(xml.Header + string(output))
}

