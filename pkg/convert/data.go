package convert

import (
	"encoding/json"
	"fmt"
	"github.com/creasty/defaults"
	"github.com/sirupsen/logrus"
	"io/ioutil"
	"strconv"
)

type Data struct {
	Sender       Sender        `json:"sender"`
	ExecuteOn    string        `json:"execute_on" default:"2020-02-10"`
	Transactions []Transaction `json:"transaction"`
}

func NewData() *Data {
	return &Data{
		Sender:       *NewSender(),
		Transactions: []Transaction{*NewTransaction()},
	}
}

func OpenData(path string) *Data {
	raw, err := ioutil.ReadFile(path)
	if err != nil {
		logrus.Fatal(err)
	}
	data := Data{}
	if err := json.Unmarshal(raw, &data); err != nil {
		logrus.Fatal(err)
	}
	return &data
}

func (d Data) Save(path string) {
	raw, err := json.Marshal(d)
	if err != nil {
		logrus.Fatal(err)
	}
	if err := ioutil.WriteFile(path, raw, 0644); err != nil {
		logrus.Fatal(err)
	}
}

func (d Data) ControlSum() string {
	sum := 0.0
	for i := range d.Transactions {
		val, err := strconv.ParseFloat(d.Transactions[i].Amount, 64)
		if err != nil {
			logrus.Fatal(err)
		}
		sum += val
	}
	return fmt.Sprintf("%.2f", sum)
}

type Sender struct {
	Name     string `json:"name" default:"Fantasia Company"`
	Street   string `json:"street" default:"Main Street"`
	StreetNr int    `json:"nr" default:"1"`
	Place    string `json:"place" default:"Zurich"`
	Postcode int    `json:"postcode" default:"8000"`
	Country  string `json:"country" default:"CH"`
	Iban     string `json:"iban" default:"CH38 4929 3842 9384 0934"`
	Bic      string `json:"bic" default:"EXP3483"`
}

func NewSender() *Sender {
	sender := &Sender{}
	if err := defaults.Set(sender); err != nil {
		logrus.Fatal(err)
	}
	return sender
}

type Transaction struct {
	Name      string `json:"name" default:"Max Musterman"`
	Reference string `json:"description" default:"Some Reference"`
	Amount    string `json:"amount" default:"00.10"`
	Street    string `json:"street" default:"Main Street"`
	Nr        int    `json:"nr" default:"2"`
	Place     string `json:"place" default:"Zurich"`
	Postcode  int    `json:"postcode" default:"8000"`
	Country   string `json:"country" default:"CH"`
	Iban      string `json:"iban" default:"CH38 4929 3842 9348 3930"`
}

func NewTransaction() *Transaction {
	transaction := &Transaction{}
	if err := defaults.Set(transaction); err != nil {
		logrus.Fatal(err)
	}
	return transaction
}
