# sba-pay

Create a ISO 20022 compatible pain.001 files according to the [Swiss standard](https://www.six-group.com/interbank-clearing/dam/downloads/en/standardization/iso/swiss-recommendations/implementation-guidelines-ct.pdf). This repo cli app (`pay-convert`) for converting JSON to pain.001 files. As well as a go library for other usages. This project is part of the toolchain we use at the [Solutionsbüro](https://buero.io) for our accounting.

## Usage

Create a new input file (`input.json`):
```
sba-pay create input.json
```

Convert `input.json` to `payments.xml`:
```
sba-pay input.json -o payments.xml
```

## Input file

You can create an default input file with `pay-convert create NAME.json`. An example for an input file:

```json
{
  "sender": {
    "name": "Fantasia Company",
    "street": "Main Street",
    "nr": 1,
    "place": "Zurich",
    "postcode": 8000,
    "country": "CH",
    "iban": "CH3483249823434",
    "bic": "EXP3423"
  },
  "transactions": [
    {
      "description": "Some Description",
      "name": "Max Musterman",
      "street": "Main Street",
      "nr": 2,
      "place": "Zurich",
      "postcode": 8000,
      "country": "CH",
      "iban": "CH3483249843893"
    }
  ]
}
```
